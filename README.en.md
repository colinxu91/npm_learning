# npm_learning

#### 介绍
This is the project for npm learning

#### npm_first_learning

1. How to init project
```
npm init --scope=colinxu91
```
2. How to publish
```
npm publish --access public
```
You need to pay if you want to publish your version as private.

3. How to update version
```
npm version patch
```

4. How to add tag for your package
```
npm publish --tag beta
```

5. How to add tag for special version of your package
```
npm dist-tag add example-package@1.4.0 stable
```
# npm_learning

#### 介绍
npm 学习代码

#### npm_first_learning

1. 如何初始化
```
npm init --scope=colinxu91
```
2. 如何发布npm库
```
npm publish --access public
```
npm私有库需要付费

3. 如何更新版本
```
npm version patch
```


4. 如何打标签
```
npm publish --tag beta
```

5. 如何给一个特定的版本打标签
```
npm dist-tag add example-package@1.4.0 stable
```
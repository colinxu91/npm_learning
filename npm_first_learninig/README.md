* You can find source code and learn how to create and publish your own package
https://gitee.com/colinxu91/npm_learning

* How to create test project
```
mkdir test && cd test && npm init
```

* How to install this module
```
npm install @colinxu91/npm_first_learning
```

* How to use this module

Create javascript file 'test.js' and add follow code in test.js
```
const learningModule = require('@colinxu91/npm_first_learning');

learningModule.printMsg();
````

* How to run test script
```
node test.js
```